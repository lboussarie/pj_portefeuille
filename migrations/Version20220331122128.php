<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220331122128 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE activite_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE competence_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE parcours_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE processus_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE production_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE promotion_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE reformulation_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE situation_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "utilisateur_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE activite (id INT NOT NULL, processus_id INT NOT NULL, nom VARCHAR(60) NOT NULL, libelle VARCHAR(100) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B8755515A55629DC ON activite (processus_id)');
        $this->addSql('CREATE TABLE competence (id INT NOT NULL, activite_id INT NOT NULL, nom VARCHAR(60) NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_94D4687F9B0F88B1 ON competence (activite_id)');
        $this->addSql('CREATE TABLE etudiant (id INT NOT NULL, promotion_id INT DEFAULT NULL, parcours_id INT DEFAULT NULL, numeroexamen VARCHAR(50) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_717E22E3139DF194 ON etudiant (promotion_id)');
        $this->addSql('CREATE INDEX IDX_717E22E36E38C0DB ON etudiant (parcours_id)');
        $this->addSql('CREATE TABLE parcours (id INT NOT NULL, nom VARCHAR(60) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE parcours_processus (parcours_id INT NOT NULL, processus_id INT NOT NULL, PRIMARY KEY(parcours_id, processus_id))');
        $this->addSql('CREATE INDEX IDX_C36AE40F6E38C0DB ON parcours_processus (parcours_id)');
        $this->addSql('CREATE INDEX IDX_C36AE40FA55629DC ON parcours_processus (processus_id)');
        $this->addSql('CREATE TABLE processus (id INT NOT NULL, nom VARCHAR(60) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE production (id INT NOT NULL, situation_id INT NOT NULL, designation VARCHAR(200) NOT NULL, uri VARCHAR(200) NOT NULL, uri_entier VARCHAR(200) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D3EDB1E03408E8AF ON production (situation_id)');
        $this->addSql('CREATE TABLE professeur (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE professeur_promotion (professeur_id INT NOT NULL, promotion_id INT NOT NULL, PRIMARY KEY(professeur_id, promotion_id))');
        $this->addSql('CREATE INDEX IDX_AD4F5DB7BAB22EE9 ON professeur_promotion (professeur_id)');
        $this->addSql('CREATE INDEX IDX_AD4F5DB7139DF194 ON professeur_promotion (promotion_id)');
        $this->addSql('CREATE TABLE promotion (id INT NOT NULL, nom VARCHAR(60) NOT NULL, annee_debut VARCHAR(10) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE reformulation (id INT NOT NULL, competence_id INT NOT NULL, situation_id INT NOT NULL, professeur_id INT NOT NULL, texte VARCHAR(255) DEFAULT NULL, valide BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E61B5BE915761DAB ON reformulation (competence_id)');
        $this->addSql('CREATE INDEX IDX_E61B5BE93408E8AF ON reformulation (situation_id)');
        $this->addSql('CREATE INDEX IDX_E61B5BE9BAB22EE9 ON reformulation (professeur_id)');
        $this->addSql('CREATE TABLE situation (id INT NOT NULL, etudiant_id INT DEFAULT NULL, commentaire_profs VARCHAR(200) DEFAULT NULL, libelle VARCHAR(60) NOT NULL, description VARCHAR(100) NOT NULL, localisation VARCHAR(60) NOT NULL, source VARCHAR(100) DEFAULT NULL, type VARCHAR(60) NOT NULL, date_debut DATE NOT NULL, date_fin DATE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_EC2D9ACADDEAB1A3 ON situation (etudiant_id)');
        $this->addSql('CREATE TABLE situation_activite (situation_id INT NOT NULL, activite_id INT NOT NULL, PRIMARY KEY(situation_id, activite_id))');
        $this->addSql('CREATE INDEX IDX_CAF0B23F3408E8AF ON situation_activite (situation_id)');
        $this->addSql('CREATE INDEX IDX_CAF0B23F9B0F88B1 ON situation_activite (activite_id)');
        $this->addSql('CREATE TABLE "utilisateur" (id INT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, typeutilisateur VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1D1C63B3E7927C74 ON "utilisateur" (email)');
        $this->addSql('ALTER TABLE activite ADD CONSTRAINT FK_B8755515A55629DC FOREIGN KEY (processus_id) REFERENCES processus (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE competence ADD CONSTRAINT FK_94D4687F9B0F88B1 FOREIGN KEY (activite_id) REFERENCES activite (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE etudiant ADD CONSTRAINT FK_717E22E3139DF194 FOREIGN KEY (promotion_id) REFERENCES promotion (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE etudiant ADD CONSTRAINT FK_717E22E36E38C0DB FOREIGN KEY (parcours_id) REFERENCES parcours (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE etudiant ADD CONSTRAINT FK_717E22E3BF396750 FOREIGN KEY (id) REFERENCES "utilisateur" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE parcours_processus ADD CONSTRAINT FK_C36AE40F6E38C0DB FOREIGN KEY (parcours_id) REFERENCES parcours (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE parcours_processus ADD CONSTRAINT FK_C36AE40FA55629DC FOREIGN KEY (processus_id) REFERENCES processus (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE production ADD CONSTRAINT FK_D3EDB1E03408E8AF FOREIGN KEY (situation_id) REFERENCES situation (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE professeur ADD CONSTRAINT FK_17A55299BF396750 FOREIGN KEY (id) REFERENCES "utilisateur" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE professeur_promotion ADD CONSTRAINT FK_AD4F5DB7BAB22EE9 FOREIGN KEY (professeur_id) REFERENCES professeur (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE professeur_promotion ADD CONSTRAINT FK_AD4F5DB7139DF194 FOREIGN KEY (promotion_id) REFERENCES promotion (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE reformulation ADD CONSTRAINT FK_E61B5BE915761DAB FOREIGN KEY (competence_id) REFERENCES competence (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE reformulation ADD CONSTRAINT FK_E61B5BE93408E8AF FOREIGN KEY (situation_id) REFERENCES situation (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE reformulation ADD CONSTRAINT FK_E61B5BE9BAB22EE9 FOREIGN KEY (professeur_id) REFERENCES professeur (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE situation ADD CONSTRAINT FK_EC2D9ACADDEAB1A3 FOREIGN KEY (etudiant_id) REFERENCES etudiant (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE situation_activite ADD CONSTRAINT FK_CAF0B23F3408E8AF FOREIGN KEY (situation_id) REFERENCES situation (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE situation_activite ADD CONSTRAINT FK_CAF0B23F9B0F88B1 FOREIGN KEY (activite_id) REFERENCES activite (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE competence DROP CONSTRAINT FK_94D4687F9B0F88B1');
        $this->addSql('ALTER TABLE situation_activite DROP CONSTRAINT FK_CAF0B23F9B0F88B1');
        $this->addSql('ALTER TABLE reformulation DROP CONSTRAINT FK_E61B5BE915761DAB');
        $this->addSql('ALTER TABLE situation DROP CONSTRAINT FK_EC2D9ACADDEAB1A3');
        $this->addSql('ALTER TABLE etudiant DROP CONSTRAINT FK_717E22E36E38C0DB');
        $this->addSql('ALTER TABLE parcours_processus DROP CONSTRAINT FK_C36AE40F6E38C0DB');
        $this->addSql('ALTER TABLE activite DROP CONSTRAINT FK_B8755515A55629DC');
        $this->addSql('ALTER TABLE parcours_processus DROP CONSTRAINT FK_C36AE40FA55629DC');
        $this->addSql('ALTER TABLE professeur_promotion DROP CONSTRAINT FK_AD4F5DB7BAB22EE9');
        $this->addSql('ALTER TABLE reformulation DROP CONSTRAINT FK_E61B5BE9BAB22EE9');
        $this->addSql('ALTER TABLE etudiant DROP CONSTRAINT FK_717E22E3139DF194');
        $this->addSql('ALTER TABLE professeur_promotion DROP CONSTRAINT FK_AD4F5DB7139DF194');
        $this->addSql('ALTER TABLE production DROP CONSTRAINT FK_D3EDB1E03408E8AF');
        $this->addSql('ALTER TABLE reformulation DROP CONSTRAINT FK_E61B5BE93408E8AF');
        $this->addSql('ALTER TABLE situation_activite DROP CONSTRAINT FK_CAF0B23F3408E8AF');
        $this->addSql('ALTER TABLE etudiant DROP CONSTRAINT FK_717E22E3BF396750');
        $this->addSql('ALTER TABLE professeur DROP CONSTRAINT FK_17A55299BF396750');
        $this->addSql('DROP SEQUENCE activite_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE competence_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE parcours_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE processus_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE production_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE promotion_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE reformulation_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE situation_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE "utilisateur_id_seq" CASCADE');
        $this->addSql('DROP TABLE activite');
        $this->addSql('DROP TABLE competence');
        $this->addSql('DROP TABLE etudiant');
        $this->addSql('DROP TABLE parcours');
        $this->addSql('DROP TABLE parcours_processus');
        $this->addSql('DROP TABLE processus');
        $this->addSql('DROP TABLE production');
        $this->addSql('DROP TABLE professeur');
        $this->addSql('DROP TABLE professeur_promotion');
        $this->addSql('DROP TABLE promotion');
        $this->addSql('DROP TABLE reformulation');
        $this->addSql('DROP TABLE situation');
        $this->addSql('DROP TABLE situation_activite');
        $this->addSql('DROP TABLE "utilisateur"');
    }
}
