<?php

namespace App\Entity;

use App\Repository\ReformulationRepository;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Boolean;

/**
 * @ORM\Entity(repositoryClass=ReformulationRepository::class)
 */
class Reformulation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $texte;

    /**
     * @ORM\Column(type="boolean")
     */
    private $valide;

    /**
     * @ORM\ManyToOne(targetEntity=Competence::class, inversedBy="reformulations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $competence;

    /**
     * @ORM\ManyToOne(targetEntity=Situation::class, inversedBy="reformulations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $situation;

    /**
     * @ORM\ManyToOne(targetEntity=Professeur::class, inversedBy="reformulations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $professeur;
    
    public function __construct(Professeur $professeur=null,Situation $situation=null,Competence $competence=null,Boolean $valide=null,string $texte=null,int $id=null)
    {
        $this->id = $id;
        $this->texte = $texte;
        $this->valide = $valide;
        $this->competence = $competence;
        $this->situation = $situation;
        $this->professeur = $professeur;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTexte(): ?string
    {
        return $this->texte;
    }

    public function setTexte(?string $texte): self
    {
        $this->texte = $texte;

        return $this;
    }

    public function getValide(): ?bool
    {
        return $this->valide;
    }

    public function setValide(bool $valide): self
    {
        $this->valide = $valide;

        return $this;
    }

    public function getCompetence(): ?Competence
    {
        return $this->competence;
    }

    public function setCompetence(?Competence $competence): self
    {
        $this->competence = $competence;

        return $this;
    }

    public function getSituation(): ?Situation
    {
        return $this->situation;
    }

    public function setSituation(?Situation $situation): self
    {
        $this->situation = $situation;

        return $this;
    }

    public function getProfesseur(): ?Professeur
    {
        return $this->professeur;
    }

    public function setProfesseur(?Professeur $professeur): self
    {
        $this->professeur = $professeur;

        return $this;
    }
}
