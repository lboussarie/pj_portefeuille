<?php

namespace App\Entity;

use App\Repository\SituationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SituationRepository::class)
 */
class Situation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $commentaire_profs;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $libelle;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $localisation;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $source;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateDebut;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateFin;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $environnementTech;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $moyens;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $avisPersonnel;

    /**
     * @ORM\ManyToOne(targetEntity=Etudiant::class, inversedBy="situations")
     */
    private $etudiant;

    /**
     * @ORM\ManyToMany(targetEntity=Activite::class)
     */
    private $activite;

    /**
     * @ORM\OneToMany(targetEntity=Reformulation::class, mappedBy="situation")
     */
    private $reformulations;
    
    /**
     * @ORM\OneToMany(targetEntity=Production::class, mappedBy="situation")
     */
    private $productions;

    public function __construct(Utilisateur $etudiant=null,int $id=null,string $commentaire_profs=null,string $libelle=null,string $description=null,string $localisation=null,string $source=null,string $type=null,\DateTime $dateDebut=null,\DateTime $dateFin=null,string $environnementTech=null, string $moyens=null, string $avisPersonnel=null)
    {
        $this->productions = new ArrayCollection();
        $this->activite = new ArrayCollection();
        $this->reformulations = new ArrayCollection();
        $this->id = $id;
        $this->commentaire_profs = $commentaire_profs;
        $this->libelle = $libelle;
        $this->description = $description;
        $this->localisation = $localisation;
        $this->source = $source;
        $this->type = $type;
        $this->dateDebut = $dateDebut;
        $this->dataFin = $dateFin;
        $this->etudiant = $etudiant;
        $this->environnementTech = $environnementTech;
        $this->moyens = $moyens;
        $this->avisPersonnel = $avisPersonnel;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCommentaireProfs(): ?string
    {
        return $this->commentaire_profs;
    }

    public function setCommentaireProfs(?string $commentaire_profs): self
    {
        $this->commentaire_profs = $commentaire_profs;

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLocalisation(): ?string
    {
        return $this->localisation;
    }

    public function setLocalisation(string $localisation): self
    {
        $this->localisation = $localisation;

        return $this;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(?string $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(\DateTimeInterface $dateDebut): self
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(\DateTimeInterface $dateFin): self
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function getEtudiant(): ?Etudiant
    {
        return $this->etudiant;
    }

    public function setEtudiant(?Etudiant $etudiant): self
    {
        $this->etudiant = $etudiant;

        return $this;
    }

    /**
     * @return Collection|Activite[]
     */
    public function getActivite(): Collection
    {
        return $this->activite;
    }

    public function addActivite(Activite $activite): self
    {
        if (!$this->activite->contains($activite)) {
            $this->activite[] = $activite;
        }

        return $this;
    }

    public function removeActivite(Activite $activite): self
    {
        $this->activite->removeElement($activite);

        return $this;
    }

    /**
     * @return Collection|Reformulation[]
     */
    public function getReformulations(): Collection
    {
        return $this->reformulations;
    }

    public function addReformulation(Reformulation $reformulation): self
    {
        if (!$this->reformulations->contains($reformulation)) {
            $this->reformulations[] = $reformulation;
            $reformulation->setSituation($this);
        }

        return $this;
    }

    public function removeReformulation(Reformulation $reformulation): self
    {
        if ($this->reformulations->removeElement($reformulation)) {
            // set the owning side to null (unless already changed)
            if ($reformulation->getSituation() === $this) {
                $reformulation->setSituation(null);
            }
        }

        return $this;
    }
    
    /**
     * @return Collection|Production[]
     */
    public function getProductions(): Collection
    {
        return $this->productions;
    }
    
    public function addProduction(Production $production): self
    {
        if (!$this->productions->contains($production)) {
            $this->productions[] = $production;
            $production->setSituation($this);
        }
        
        return $this;
    }
    
    public function removeProduction(Production $production): self
    {
        if ($this->productions->removeElement($production)) {
            // set the owning side to null (unless already changed)
            if ($production->getSituation() === $this) {
                $production->setSituation(null);
            }
        }
        
        return $this;
    }

    public function getEnvironnementTech(): ?string
    {
        return $this->environnementTech;
    }

    public function setEnvironnementTech(?string $environnementTech): self
    {
        $this->environnementTech = $environnementTech;

        return $this;
    }

    public function getMoyens(): ?string
    {
        return $this->moyens;
    }

    public function setMoyens(?string $moyens): self
    {
        $this->moyens = $moyens;

        return $this;
    }

    public function getAvisPersonnel(): ?string
    {
        return $this->avisPersonnel;
    }

    public function setAvisPersonnel(?string $avisPersonnel): self
    {
        $this->avisPersonnel = $avisPersonnel;

        return $this;
    }
}
