<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\EtudiantRepository;

/**
 * @ORM\Entity(repositoryClass=EtudiantRepository::class)
 */
class Etudiant extends Utilisateur
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $numeroexamen;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Promotion", inversedBy="etudiants")
     * @ORM\JoinColumn(nullable=true)
     */
    private $promotion;

    /**
     * @ORM\OneToMany(targetEntity=Situation::class, mappedBy="etudiant")
     */
    private $situations;

    /**
     * @ORM\ManyToOne(targetEntity=Parcours::class, inversedBy="etudiants")
     * @ORM\JoinColumn(nullable=true)
     */
    private $parcours;

    public function __construct()
    {
        $this->situations = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getPromotion()
    {
        return $this->promotion;
    }

    /**
     * @param mixed $promotion
     */
    public function setPromotion($promotion)
    {
        $this->promotion = $promotion;
    }

    public function getIdEtudiant(): ?int
    {
        return $this->id;
    }

    public function getNumeroexamen(): ?string
    {
        return $this->numeroexamen;
    }

    public function setNumeroexamen(string $numeroexamen): self
    {
        $this->numeroexamen = $numeroexamen;

        return $this;
    }

    /**
     * @return Collection|Situation[]
     */
    public function getSituations(): Collection
    {
        return $this->situations;
    }

    public function addSituation(Situation $situation): self
    {
        if (!$this->situations->contains($situation)) {
            $this->situations[] = $situation;
            $situation->setEtudiant($this);
        }

        return $this;
    }

    public function removeSituation(Situation $situation): self
    {
        if ($this->situations->removeElement($situation)) {
            // set the owning side to null (unless already changed)
            if ($situation->getEtudiant() === $this) {
                $situation->setEtudiant(null);
            }
        }

        return $this;
    }

    public function getParcours(): ?Parcours
    {
        return $this->parcours;
    }

    public function setParcours(?Parcours $parcours): self
    {
        $this->parcours = $parcours;

        return $this;
    }

    public function __toString()
    {
        return $this->getNom().' '.$this->getPrenom();
    }

}
