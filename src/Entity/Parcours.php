<?php

namespace App\Entity;

use App\Repository\ParcoursRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ParcoursRepository::class)
 */
class Parcours
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $nom;

    /**
     * @ORM\OneToMany(targetEntity=Etudiant::class, mappedBy="parcours")
     */
    private $etudiants;

    /**
     * @ORM\ManyToMany(targetEntity=Processus::class, inversedBy="parcours")
     */
    private $processus;

    public function __construct(int $id=null,string $nom=null)
    {
        $this->etudiants = new ArrayCollection();
        $this->processus = new ArrayCollection();
        $this->id = $id;
        $this->nom = $nom;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection|Etudiant[]
     */
    public function getEtudiants(): Collection
    {
        return $this->etudiants;
    }

    public function addEtudiant(Etudiant $etudiant): self
    {
        if (!$this->etudiants->contains($etudiant)) {
            $this->etudiants[] = $etudiant;
            $etudiant->setParcours($this);
        }

        return $this;
    }

    public function removeEtudiant(Etudiant $etudiant): self
    {
        if ($this->etudiants->removeElement($etudiant)) {
            // set the owning side to null (unless already changed)
            if ($etudiant->getParcours() === $this) {
                $etudiant->setParcours(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Processus[]
     */
    public function getProcessus(): Collection
    {
        return $this->processus;
    }

    public function addProcessus(Processus $processu): self
    {
        if (!$this->processus->contains($processu)) {
            $this->processus[] = $processu;
        }

        return $this;
    }

    public function removeProcessus(Processus $processu): self
    {
        $this->processus->removeElement($processu);

        return $this;
    }

    public function __toString()
    {
        return $this->nom;
    }

}
