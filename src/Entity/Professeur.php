<?php

namespace App\Entity;

use App\Repository\ProfesseurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProfesseurRepository::class)
 */
class Professeur extends Utilisateur
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity=Promotion::class, inversedBy="professeurs")
     */
    private $promotion;

    /**
     * @ORM\OneToMany(targetEntity=Reformulation::class, mappedBy="professeur")
     */
    private $reformulations;

    public function __construct()
    {
        $this->promotion = new ArrayCollection();
        $this->reformulations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Promotion[]
     */
    public function getPromotion(): Collection
    {
        return $this->promotion;
    }

    public function addPromotion(Promotion $promotion): self
    {
        if (!$this->promotion->contains($promotion)) {
            $this->promotion[] = $promotion;
        }

        return $this;
    }

    public function removePromotion(Promotion $promotion): self
    {
        $this->promotion->removeElement($promotion);

        return $this;
    }

    /**
     * @return Collection|Reformulation[]
     */
    public function getReformulations(): Collection
    {
        return $this->reformulations;
    }

    public function addReformulation(Reformulation $reformulation): self
    {
        if (!$this->reformulations->contains($reformulation)) {
            $this->reformulations[] = $reformulation;
            $reformulation->setProfesseur($this);
        }

        return $this;
    }

    public function removeReformulation(Reformulation $reformulation): self
    {
        if ($this->reformulations->removeElement($reformulation)) {
            // set the owning side to null (unless already changed)
            if ($reformulation->getProfesseur() === $this) {
                $reformulation->setProfesseur(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getPrenom().' '.$this->getNom();
    }

}
