<?php

namespace App\Entity;

use App\Repository\CompetenceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CompetenceRepository::class)
 */
class Competence
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Activite::class, inversedBy="competences")
     * @ORM\JoinColumn(nullable=false)
     */
    private $activite;
    
    /**
     * @ORM\OneToMany(targetEntity=Reformulation::class, mappedBy="competence")
     */
    private $reformulations;

    /**
     * @return mixed
     */
    public function getReformulations()
    {
        return $this->reformulations;
    }

    /**
     * @param mixed $reformulations
     */
    public function setReformulations($reformulations)
    {
        $this->reformulations = $reformulations;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getActivite(): ?Activite
    {
        return $this->activite;
    }

    public function setActivite(?Activite $activite): self
    {
        $this->activite = $activite;

        return $this;
    }

    public function __toString()
    {
        return $this->nom;
    }
}
