<?php

namespace App\Entity;

use App\Repository\ProductionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductionRepository::class)
 */
class Production
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $designation;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $uri;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $uri_entier;
    
    /**
    * @ORM\ManyToOne(targetEntity=Situation::class, inversedBy="productions")
    * @ORM\JoinColumn(nullable=false)
    */
    private $situation;

    /**
     * Production constructor.
     * @param $id
     * @param $designation
     * @param $uri
     * @param $uri_entier
     * @param $situation
     */
    public function __construct($id = null, $designation = null, $uri = null, $uri_entier = null, $situation = null)
    {
        $this->id = $id;
        $this->designation = $designation;
        $this->uri = $uri;
        $this->uri_entier = $uri_entier;
        $this->situation = $situation;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    public function getUri(): ?string
    {
        return $this->uri;
    }

    public function setUri(string $uri): self
    {
        $this->uri = $uri;

        return $this;
    }
    
    public function getSituation(): ?Situation
    {
        return $this->situation;
    }
    
    public function setSituation(?Situation $situation): self
    {
        $this->situation = $situation;
        
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUriEntier()
    {
        return $this->uri_entier;
    }

    /**
     * @param mixed $uri_entier
     */
    public function setUriEntier($uri_entier): void
    {
        $this->uri_entier = $uri_entier;
    }

}
