<?php
namespace App\Service;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;
class FileUploader
{
    private $targetDirectory;
    private $slugger;
    public function __construct($targetDirectory)
    {
        $this->targetDirectory = $targetDirectory;
    }
    public function upload(UploadedFile $file,String $dossier)
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $fileName = $originalFilename.'-'.date('Y-m-d').'-'.rand(1,10000).'.'.$file->getClientOriginalExtension();
        try {
            $file->move($this->getTargetDirectory().'/'.$dossier, $fileName);
        } catch (FileException $e) {
            // ... handle exception if something happens during file upload
            return $e; // for example
        }
        return [
            'nomEntier' => $fileName,
            'nom' => $originalFilename,
            ];
    }
    
    public function supp(String $file){
        unlink($file);
    }
    
    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }
}