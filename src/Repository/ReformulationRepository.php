<?php

namespace App\Repository;

use App\Entity\Reformulation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Reformulation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Reformulation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Reformulation[]    findAll()
 * @method Reformulation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReformulationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Reformulation::class);
    }

    // /**
    //  * @return Reformulation[] Returns an array of Reformulation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Reformulation
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
