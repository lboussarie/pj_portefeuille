<?php

namespace App\Form;

use App\Entity\Situation;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SituationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('libelle')
            ->add('description')
            ->add('localisation')
            ->add('source')
            ->add('type')
            ->add('environnementTech')
            ->add('moyens')
            ->add('avisPersonnel')
            ->add('dateDebut')
            ->add('dateFin')
            ->add('commentaire_profs', TextareaType::class, ['required' => false])
            ->add('retour', SubmitType::class, ['label' => 'Retour', 'validate' => false])
			->add('enregistrer', SubmitType::class, ['label' => 'Enregistrer'])
			->add('activite', SubmitType::class, ['label' => 'Ajout Activite'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Situation::class,
        ]);
    }
}
