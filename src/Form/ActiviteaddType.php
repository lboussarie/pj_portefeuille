<?php

namespace App\Form;

use App\Entity\Activite;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Situation;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ActiviteaddType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $activites = $options['activites'];
        $builder
            ->add('activite', ChoiceType::class, ['choices' => $activites,'choice_label' => 'nom'])
            ->add('submit', SubmitType::class, ['label' => 'Enregistrer'])
            ->add('retour', SubmitType::class, ['label' => 'Retour', 'validate' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'activites' => array(),
        ]);
    }
}
