<?php

namespace App\Form;

use App\Entity\Utilisateur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AjoutUtilisateurType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('file', FileType::class, ['label' => 'Ajouter des utilisateurs', 'mapped' => false, 'required' => false])
        ->add('choix', ChoiceType::class, ['choices' => ['Etudiant' => 0, 'Professeur' => 1], 'expanded' => true, 'multiple' => false, 'choice_value' => function ($currentChoiceKey) {
            return $currentChoiceKey ? 'true' : 'false';
        },])
        ->add('valider', SubmitType::class, ['label' => 'Valider'])
        ;
    }
    
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            /*'data_class' => Utilisateur::class,*/
        ]);
    }
}