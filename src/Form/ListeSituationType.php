<?php

namespace App\Form;

use App\Entity\Situation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Button;

class ListeSituationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('activite', SubmitType::class, ['label' => 'Activité', 'validate' => false])
        ->add('competence', SubmitType::class, ['label' => 'Compétence', 'validate' => false])
        ->add('production', SubmitType::class, ['label' => 'Production', 'validate' => false])
        ->add('modifier', SubmitType::class, ['label' => 'Modifier', 'validate' => true])
        ->add('supprimer', SubmitType::class, ['label' => 'Supprimer', 'validate' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Situation::class,
        ]);
    }
}
