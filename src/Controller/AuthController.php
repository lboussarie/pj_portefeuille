<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Authentication\Provider\UserAuthenticationProvider;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use App\Entity\Utilisateur;
use App\Entity\Promotion;
use Symfony\Config\Framework\RateLimiter\LimiterConfig;
use Symfony\Component\RateLimiter\LimiterInterface;

class AuthController extends AbstractController
{
    /**
     * @Route("/", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        
        //permet d'afficher accueil si l'utilisateur est déja connecter
        if ($this->getUser()) {
            return $this->redirectToRoute('accueil');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
    
    /**
     * @Route("/user/migrate_all", name="migrate_all")
     */
    public function migrate_all(Request $req,UserPasswordHasherInterface $hasher) {
        $doctrine = $this->getDoctrine();
        $manager = $doctrine->getManager();
        $repoUser = $manager->getRepository(Utilisateur::class);
        $Utilisateur = $repoUser->findAll();
        
        foreach ($Utilisateur as $u){
            if(!preg_match('#^[$2y$]#', $u->getPassword())){
                $u->setPassword($hasher->hashPassword($u, $u->getPassword()));
                $manager->persist($u);
            }
        }
        $manager->flush();
        
        return $this->redirectToRoute('accueil');
    }
}
