<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;
use App\Entity\Utilisateur;
use App\Form\FormSauvegardeType;
use App\Entity\Situation;
use App\Entity\Etudiant;
use App\Entity\Reformulation;
use App\Entity\Competence;
use App\Entity\Activite;

class SauvegardeController extends AbstractController
{
    /**
     * @Route("/sauvegarde", name="sauvegarde")
     */
    public function index(): Response
    {
        
        
        return $this->render('sauvegarde/gestionsauvegarde.html.twig');
    }
    //lien site tuto : https://symfonycasts.com/screencast/symfony2-ep3/csv-download
   
    /**
     * @Route("/sauvegarde/gestionnairesauvegarde", name="gestionnairesauvegarde")
     */
    public function gestionnairesauvegarde(Request $request){
        
       
        $form = $this->createForm(FormSauvegardeType::class);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid() ){
//Appel du manager
         $doc = $this->getDoctrine();
         $man = $doc->getManager();
        
//Appel de reporsitory
         $repoU = $man->getRepository(Utilisateur::class);
         $repoE = $man->getRepository(Etudiant::class);
         $repoS = $man->getRepository(Situation::class);
         $repoA = $man->getRepository(Activite::class);
            
//on récupère le user connecter    
        $user = $this->getUser();
        
//on récupère l'objet édutiant
        $etudiant = $repoE->find($user);
        $etudiantbis = $repoE->getDataEtudiant($etudiant);
        
//on récup les données de l'utilisateur 
//        $u = $repoU->find($user);
//        $listedataUtilisateur = $repoU->getDataUtilisateur($u);
        
//on récup la situation de l'utilisateur 
        $situationbis = $repoS->find($etudiant);
//on récupére la situation de l'étudiant
        $situation = $etudiant->getSituations();
        
//on récupère les reformulations
        $reformulation = $situationbis->getReformulations();
        
//on récupère l'activite du user
        $activite = $repoA->find($user);
//on récupère ses compètences
        $competence = $activite->getCompetences();
        
      
        
        $rows = array();
       //récupération des données de l'utilisateur
      /*  foreach ($listedataUtilisateur as $data) {
            $dataUtilisateur = array($data->getId(), $data->getNom(), $data->getPrenom(),
                $data->getEmail());
        
            
        }*/
        foreach ($etudiantbis as $dataE) {
            $dataEtudiant = array($dataE->getId(), $dataE->getNumeroexamen())
                ;
            
            
        }
        
        //récupération des données de la situation
        foreach($situation as $dataS){
            
            $dataSituation = array($dataS->getId(),$dataS->getLibelle(),$dataS->getDescription(),$dataS->getLocalisation(),$dataS->getSource(),
                $dataS->getType(), $dataS->getDateDebut()->format('Y-m-d'), $dataS->getDateFin()->format('Y-m-d'),
            );
        }
        //récupération des sonnées de la reformulation
        foreach($reformulation as $dataR){
                
            $dataReformulation = array($dataR->getId(),$dataR->getTexte(),$dataR->getValide()
                );
            
        }
        //récupération des données des compétence
        foreach($competence as $dataC){
            
            $dataCompetence = array($dataC->getId(),$dataC->getDescription()
            );
            
        }
        
        //$rows[] = implode(',', $dataUtilisateur);
        $rows[] = implode(',', $dataEtudiant);
        $rows[] = implode(',', $dataSituation);
        $rows[] = implode(',', $dataReformulation);
        $rows[] = implode(',', $dataCompetence);
        
        $content = implode("\n", $rows);
        $response = new Response($content);
        $response->headers->set('Content-Type', 'text/csv');
        }else{
            $response = $this->render('sauvegarde/gestionsauvegarde.html.twig', [
                'form' => $form->createView() ]);
        }
        return $response;
        
    }
}
