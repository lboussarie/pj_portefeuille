<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\User;
use App\Entity\Utilisateur;
use App\Form\ProfilUtilisateurType;
use Symfony\Component\HttpFoundation\Request;

class ProfilController extends AbstractController
{ 
    
    /**
     * @Route("/profil", name="profil")
     */
    public function recuperationprofil(Request $req, UserPasswordHasherInterface $hasher)
    {
        
        $doc = $this->getDoctrine();
        $man = $doc->getManager();
        
        $user= $this->getUser();
        
        $repoU = $man ->getRepository(Utilisateur::class);
        $u = $repoU ->find($user);
        
        
        
        $form = $this->createForm(ProfilUtilisateurType::class, $u);
        $form->handleRequest($req);
        
        
        if ($form->isSubmitted() && $form->isValid()) {
            //traitement données
            $doc = $this->getDoctrine();
            $man = $doc->getManager();
            $u->setPassword($hasher->hashPassword($u, $u->getPassword()));
            $man->persist($u);
            $man->flush();
            //vue
            $ret = $this->redirectToRoute('profil');
            
        } else {
            //affichage formulaire
            $ret = $this->render('profil/profil.html.twig', [
                'form' => $form->createView(),
            ]);
        }
        
        return $ret;
    }    
}
