<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use App\Entity\Etudiant;
use App\Form\SynthesePdfType;
use App\Form\SyntheseProfType;

class SynthesePdfController extends AbstractController
{
    /**
     * @Route("/synthese/pdf", name="synthese_pdf")
     */
    public function synthese_pdf(Request $req): Response
    {
        $form = $this->createForm(SynthesePdfType::class);
        $form->handleRequest($req);
        
        if($form->isSubmitted() && $form->isValid()){
            if($form->get('ajouter')->isClicked()){
                $user = $this->getUser();
                $situations = $user->getSituations();
                
                $content = $this->render('synthese_pdf/synthese.html.twig', ['user' => $user, 'activite' => $situations]);
                
                try {
                    $pdf = new Html2Pdf('L', 'A4', 'fr', true, 'UTF-8', array(10, 15, 10, 15));
                    $pdf->writeHTML($content);
                    $pdf->Output('Competence.pdf','D');
                }
                catch (Html2PdfException $e) {
                    die($e);
                }
            }
        }
        
        return $this->render('/synthese_pdf/affichage.html.twig', ['form' => $form->createView(),]);
        
    }
    
    /**
     * @Route("/synthese/pdfeleve", name="synthese_pdfeleve")
     */
    public function synthese_pdfeleve(Request $req): Response
    {
        $form = $this->createForm(SyntheseProfType::class);
        $form->handleRequest($req);
        
        if($form->isSubmitted() && $form->isValid()){
            if($form->get('ajouter')->isClicked()){
                $id = $form->get('etudiant')->getData();
                $doctrine = $this->getDoctrine();
                $manager = $doctrine->getManager();
                $repoEtu = $manager->getRepository(Etudiant::class);
                $user = $repoEtu->find($id);
                $situations = $user->getSituations();
                
                $content = $this->render('synthese_pdf/synthese.html.twig', ['user' => $user, 'activite' => $situations]);
                
                try {
                    $pdf = new Html2Pdf('L', 'A4', 'fr', true, 'UTF-8', array(10, 15, 10, 15));
                    $pdf->writeHTML($content);
                    $pdf->Output('Competence.pdf','D');
                }
                catch (Html2PdfException $e) {
                    die($e);
                }
            }
        }
        
        return $this->render('/synthese_pdf/affichagepareleve.html.twig', ['form' => $form->createView(),]);
    }
    
    /**
     * @Route("/synthese/pdfeleveid/{id}", name="synthese_pdfeleveid")
     */
    public function synthese_pdfeleveid(Request $req,int $id): Response
    {
                    $doctrine = $this->getDoctrine();
                    $manager = $doctrine->getManager();
                    $repoEtu = $manager->getRepository(Etudiant::class);
                    $user = $repoEtu->find($id);
                    $situations = $user->getSituations();
                    
                    $content = $this->render('synthese_pdf/synthese.html.twig', ['user' => $user, 'activite' => $situations]);
                    
                    try {
                        $pdf = new Html2Pdf('L', 'A4', 'fr', true, 'UTF-8', array(10, 15, 10, 15));
                        $pdf->writeHTML($content);
                        $pdf->Output('Competence.pdf','D');
                    }
                    catch (Html2PdfException $e) {
                        die($e);
                    }
        
        return $this->render('/synthese_pdf/affichagepareleve.html.twig', ['form' => $form->createView(),]);
    }
}
