<?php

namespace App\Controller;

use App\Form\ActiviteType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Activite;
use App\Entity\Processus;
use App\Entity\Competence;
use App\Entity\Situation;
use App\Entity\Reformulation;
use App\Form\ReformulationType;
use App\Form\ReformulationsType;
use App\Form\ActiviteaddType;

class ActiviteController extends AbstractController
{
    /**
     * @Route("/activite/ajoutActivite", name="ajoutActivite")
     */
    public function ajoutActivite(Request $req): Response
    {
        
        
//         <a href="{{ path('ajoutActivite') }}"><p>Ajout Activite</p>
                $doc = $this->getDoctrine();
                $man = $doc->getManager();
        
                //construction de la liste des processus
                $repoP = $man->getRepository(Processus::class);
                $listeProcessus = $repoP->findAll();
                
                $a = new Activite();
                $form = $this->createForm(ActiviteType::class, $a, ['liste_processus'=>$listeProcessus]);
                    $form->handleRequest($req);
                    if ($form->isSubmitted()) {
                        //traitement données
                        $man->persist($a);
                        $man->flush();
        
                        $ret = $this->redirectToRoute('gestionprocessus');
        
                    } else {
                        //formulaire non validé donc affichage formulaire
                        $ret = $this->render('activite/add.html.twig', [
                            'form' => $form->createView(), 
                        ]);
                    }
        
                    return $ret;
        
        $form = $this->createForm(ActiviteType::class);
        
        return $this->render('activite/add.html.twig', [
            'form' => $form->createView()
        ]);
    }
    
    /**
     * @Route("/activite/editActivite/{id}", name="editActivite")
     */
    public function editActivite(Request $req, int $id): Response
    {
        
        $doc = $this->getDoctrine();
        $man = $doc->getManager();
        
        $a = new Activite();
        
        $repoA = $man->getRepository(Activite::class);
        $a = $repoA->find($id);
        
        //construction de la liste des processus
        $repoP = $man->getRepository(Processus::class);
        $listeProcessus = $repoP->findAll();
        
        $repoC = $man->getRepository(Competence::class);
        $listeCompetence = $repoC->findBy(['activite' => $a]);
        
        $form = $this->createForm(ActiviteType::class, $a, ['liste_processus'=>$listeProcessus]);
        $form->handleRequest($req);
        if ($form->isSubmitted()) {
            //traitement données
            $man->persist($a);
            $man->flush();
            
            $ret = $this->redirectToRoute('gestionprocessus');
            
        } else {
            //formulaire non validé donc affichage formulaire
            $ret = $this->render('activite/add.html.twig', [
                'form' => $form->createView(),
                'listeCompetence'=>$listeCompetence,
                'id' => $id,
            ]);
        }
        
        return $ret;
        
        $form = $this->createForm(ActiviteType::class);
    }
    
    /**
     * @Route("/activite/deleteActivite/{id}", name="deleteActivite")
     */
    public function deleteActivite(Request $req, int $id): Response
    {
        $doc = $this->getDoctrine();
        $man = $doc->getManager();
        
        $a = new Activite();
        
        $repoA = $man->getRepository(Activite::class);
        $a = $repoA->find($id);
        
        $competences = $a->getCompetences();
        
        foreach ($competences as $c){
            $reformulations = $c->getReformulations();
        }
        
        /*foreach ($reformulations as $r){
            $man->remove($r);
        }*/
        
        foreach($competences as $c){
            $man->remove($c);
        }
        
        $man->remove($a);
        $man->flush();
        
        
        return $this->redirectToRoute('gestionprocessus');
    }
    
    /**
     * @Route("/activite/activiteParSituation/{id}", name="activiteParSituation")
     */
    public function activiteParSituation(Request $req,int $id): Response
    {
        serialize($this->getUser());
        $listeActivite=[];
        $listeCompetences=[];
        $listeReformulation=[];
        $doctrine = $this->getDoctrine();
        $manager = $doctrine->getManager();
        $repoSitu = $manager->getRepository(Situation::class);
        $situation = $repoSitu->find($id);
        
        $listeActivite = $situation->getActivite();
        
        foreach ($listeActivite as $la){
            $listeCompetences = $la->getCompetences();
        }
        
        foreach ($listeCompetences as $lc){
            $listeReformulation = $lc->getReformulations();
        }
        
        $form = $this->createForm(ReformulationType::class, $listeReformulation);
        $form->handleRequest($req);
        if ($form->isSubmitted()){
            foreach ($listeReformulation as $ref){
                $manager->persist($ref);
            }
            $manager->flush();
        }
        
        return $this->render('activite/liste.html.twig', [
            'listeSituation' => $situation,
            'listeActivite' => $listeActivite,
            'listeCompetence' => $listeCompetences,
            'listeReformulation' => $listeReformulation,
            'form' => $form->createView(),
        ]);
    }
    
    
    /**
     * @Route("/activite/ajoutactivitesitu/{id}", name="ajoutactivitesitu")
     */
    public function ajoutactivitesitu(Request $req,int $id): Response
    {
        serialize($this->getUser());
        $doctrine = $this->getDoctrine();
        $manager = $doctrine->getManager();
        $repoActi = $manager->getRepository(Activite::class);
        $activites = $repoActi->findAll();
        
        $repoSitu = $manager->getRepository(Situation::class);
        $situation = $repoSitu->find($id);
        
        $activitesituation = $situation->getActivite();
        
        foreach ($activites as $a){
            foreach ($activitesituation as $as){
                if($a == $as){
                    unset($activites[array_search($as, $activites)]);
                }
            }
        }
        
        $form = $this->createForm(ActiviteaddType::class,null,['activites' => $activites]);
        $form->handleRequest($req);
        
        if($form->isSubmitted()){
            if($form->get('retour')->isClicked()){
                $reponse = $this->redirectToRoute('affichersituation');
            }
            else {
                $situation->addActivite($form->get('activite')->getData());
                $manager->persist($situation);
                $manager->flush();
                
                $reponse = $this->redirectToRoute('ajoutactivitesitu', ['id' => $id]);
            }
        }
        else {
            $reponse = $this->render('activite/ajoutactivitesitu.html.twig', 
                ['form' => $form->createView(),
                    'activites' => $activitesituation,
                ]);
        }
        
        return $reponse;
    }
}
