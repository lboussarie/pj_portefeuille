<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Etudiant;
use App\Entity\Production;
use App\Entity\Situation;
use App\Form\SituationType;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\EtudiantRepository;
use App\Form\ListeSituationType;
use App\Entity\Reformulation;
use App\Form\ProductionType;
use App\Form\ReformulationsType;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Service\FileUploader;
use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;

class ProductionController extends AbstractController
{
    
    /**
     * @Route("/production/ajoutproduction/{id}", name="ajoutproduction")
     */
    public function ajoutproduction(Request $req,int $id, FileUploader $file_uploader): Response
    {
        $user = $this->getUser();
        serialize($user);
        $doctrine = $this->getDoctrine();
        $manager = $doctrine->getManager();
        $repoSitu = $manager->getRepository(Situation::class);
        $situation = $repoSitu->find($id);

        $production = new Production();
        $form = $this->createForm(ProductionType::class);
        $form->handleRequest($req);

        if($form->isSubmitted() && $form->isValid()){
            $doss = $user->getNom().$user->getPrenom().$user->getId().$user->getPromotion()->getNom().$user->getParcours()->getNom();
            $dossier = $this->getParameter('upload_directory').'\\'.$doss;
            if (!file_exists($dossier) && !is_dir($dossier)){
                mkdir($dossier,0777);
            }
            $file = $form->get('uri')->getData();
            $file_name = $file_uploader->upload($file,$doss);
            $production->setDesignation($form->get('designation')->getData());
            $production->setUri($file_name['nom']);
            $production->setUriEntier($file_name['nomEntier']);
            $production->setSituation($situation);
            $manager->persist($production);
            $manager->flush();

            $reponse = $this->redirectToRoute('afficherproduction', ['id' => $id]);
        }
        else {
            $reponse = $this->render('production/ajoutproduction.html.twig', [
                'form' => $form->createView(),
            ]);
        }

        return $reponse;
    }

    /**
     * @Route("/production/supprimerproduction/{id}{situationid}", name="supprimerproduction")
     */
    public function supprimerproduction(int $id,int $situationid): Response
    {
        $user = $this->getUser();
        $doctrine = $this->getDoctrine();
    	$manager = $doctrine->getManager();
    	$repoProd = $manager->getRepository(Production::class);
    	$production = $repoProd->find($id);
    	$uri = $production->getUriEntier();
    	
    	$dossier = $user->getNom().$user->getPrenom().$user->getId().$user->getPromotion()->getNom().$user->getParcours()->getNom();
    	$route = $this->getParameter('upload_directory').'/'.$dossier.'/'.$uri;
    	if (!file_exists($dossier)){
    	    unlink($route);
    	}
    	
        $manager->remove($production);
        $manager->flush();

        return $this->redirectToRoute('afficherproduction', ['id' => $situationid]);
    }

    /**
     * @Route("/production/modifierproduction/{id}{situationid}", name="modifierproduction")
     */
    public function modifierproduction(Request $req,int $id,int $situationid,FileUploader $file_uploader): Response
    {
        $user = $this->getUser();
        serialize($user);
    	$doctrine = $this->getDoctrine();
    	$manager = $doctrine->getManager();
    	$repoProd = $manager->getRepository(Production::class);
    	$production = $repoProd->find($id);
    	
    	$form = $this->createForm(ProductionType::class,$production);
    	$form->handleRequest($req);
    	
    	$dossier = $user->getNom().$user->getPrenom().$user->getId().$user->getPromotion()->getNom().$user->getParcours()->getNom();
    	$last_file = $production->getUriEntier();
    	
    	if($form->isSubmitted() && $form->isValid()){
    	    /*if($form->get('retour')->isClicked()){
    	        $reponse = $this->redirectToRoute('afficherproduction', ['id' => $situationid]);
    	    }
    	    else {*/
    	    $uri = $form->get('uri')->getData();
    	    $ancien_file = $this->getParameter('upload_directory').'/'.$dossier.'/'.$last_file;
    	    $file_uploader->supp($ancien_file);
    	    $file_name = $file_uploader->upload($uri,$dossier);
    	    $production->setUri($file_name['nom']);
    	    $production->setUriEntier($file_name['nomEntier']);
    	    $manager->persist($production);
    	    $manager->flush();
    	    $reponse = $this->redirectToRoute('afficherproduction', ['id' => $situationid]);
    	}
    	else {
    	    $reponse = $this->render('production/modifierproduction.html.twig', ['form' => $form->createView()]);
    	}
    	
    	return $reponse;
    }

    /**
     * @Route("/production/afficherproduction/{id}", name="afficherproduction")
     */
    public function afficherproduction(Request $req,int $id): Response
    {
        serialize($this->getUser());
        $doctrine = $this->getDoctrine();
        $manager = $doctrine->getManager();
        $repoSitu = $manager->getRepository(Situation::class);
        $situation = $repoSitu->find($id);
        $productions = $situation->getProductions();
        
        return $this->render('production/afficherproduction.html.twig', [
            'situation' => $situation,
            'productions' => $productions,
        ]);
    }
    
    /**
     * @Route("/production/telechargement/{nom}", name="telechargement")
     */
    public function telechargement(Request $req,string $nom): Response
    {
        $user = $this->getUser();
        serialize($user);
        $doss = $user->getNom().$user->getPrenom().$user->getId().$user->getPromotion()->getNom().$user->getParcours()->getNom();
        $file = $this->getParameter('upload_directory').'/'.$doss.'/'.$nom;
        return $this->file($file);
    }

    /**
     * @Route("/production/telechargement/{nom}{prenom}{id}{promotion}{parcours}/{uri}", name="telechargement_prof")
     */
    public function telechargementProf(Request $req,string $nom,string $prenom,string $id,string $uri,string $promotion,string $parcours): Response
    {
        $user = $this->getUser();
        serialize($user);
        $doss = $nom.$prenom.$id.$promotion.$parcours;
        $file = $this->getParameter('upload_directory').'/'.$doss.'/'.$uri;
        return $this->file($file);
    }
}
