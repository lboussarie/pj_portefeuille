<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Parcours;
use App\Form\ParcoursType;
use App\Entity\Processus;

class ParcoursController extends AbstractController
{
    /**
     * @Route("/parcours/ajoutparcours", name="ajoutparcours")
     */
    public function ajoutparcours(Request $req): Response
    {
        $doctrine = $this->getDoctrine();
        $manager = $doctrine->getManager();
        
        $parcours = new Parcours();
        $form = $this->createForm(ParcoursType::class,$parcours);
        $form->handleRequest($req);
        
        if($form->isSubmitted() && $form->isValid()){
            if($form->get('retour')->isClicked()){
                $reponse = $this->redirectToRoute('gestionparcours');
            }
            else {
                $manager->persist($parcours);
                $manager->flush();
                
                $reponse = $this->redirectToRoute('gestionparcours');
            }
        }
        else {
            $reponse = $this->render('parcours/ajoutparcours.html.twig', [
                'form' => $form->createView(),
            ]);
        }
        return $reponse;
    }
    
    /**
     *  @Route("/parcours/modifierparcours/{id}", name="modifierparcours")
     */
     public function modifierparcours(Request $req,int $id) {
	 
	    $doctrine = $this->getDoctrine();
    	$manager = $doctrine->getManager();
    	$repoParc = $manager->getRepository(Parcours::class);
    	$parcours = $repoParc->find($id);
    	
    	$form = $this->createForm(ParcoursType::class,$parcours);
    	$form->handleRequest($req);
    	
    	if($form->isSubmitted() && $form->isValid()){
    	    if($form->get('retour')->isClicked()){
    	        $reponse = $this->redirectToRoute('gestionparcours');
    	    }
    	    else {
    	        $manager->persist($parcours);
    	        $manager->flush();
    	        
    	        $reponse = $this->redirectToRoute('gestionparcours');
    	    }
    	}
    	else {
    	    $reponse = $this->render('parcours/modifierparcours.html.twig', ['form' => $form->createView()]);
    	}
    	
    	return $reponse;
    }
    
    /**
     *  @Route("/parcours/supprimerparcours/{id}", name="supprimerparcours")
     */
     public function supprimerparcours(int $id) {
	 
	    $listeetudiants = [];
        $doctrine = $this->getDoctrine();
    	$manager = $doctrine->getManager();
    	$repoParc = $manager->getRepository(Parcours::class);
        $parcours = $repoParc->find($id);
        
        $listeetudiants = $parcours->getEtudiants();
        
        foreach ($listeetudiants as $li){
            $parcours->removeEtudiant($li);
        }
        
        $manager->remove($parcours);
        $manager->flush();

        return $this->redirectToRoute('gestionparcours');
     }
     
     /**
      * @Route("/parcours/gestionparcours", name="gestionparcours")
      */
     public function gestionparcours(Request $req): Response
     {
         $doctrine = $this->getDoctrine();
         $manager = $doctrine->getManager();
         $repoParc = $manager->getRepository(Parcours::class);
         $parcours = $repoParc->findAll();
         
         return $this->render('parcours/gestionparcours.html.twig', ['parcours' => $parcours]);
     }
}
