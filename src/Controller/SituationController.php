<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Etudiant;
use App\Entity\Production;
use App\Entity\Situation;
use App\Form\SituationType;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\EtudiantRepository;
use App\Form\ListeSituationType;
use App\Entity\Reformulation;
use App\Form\ReformulationsType;
use App\Form\ReturnButtonType;
use App\Form\BoutonAjoutSituationType;
use App\Entity\Parcours;

class SituationController extends AbstractController
{
    
    /**
     * @Route("/situation/listesituationspromo", name="listesituationspromo")
     */
    public function listesituationspromo(Request $req): Response
    {
        $doctrine = $this->getDoctrine();
        $manager = $doctrine->getManager();

        $repoSitu = $manager->getRepository(Situation::class);
        $situations = $repoSitu->findAll();

        return $this->render('situation/listesituationpromo.html.twig', [
            'situations' => $situations,
        ]);
    }
    
    /**
     * Permet d'afficher toute les situations d'un Etudiant
     *
     * @param req 
     *
     * @return Vue
     * @author Louis Boussarie
     * @version 1.0
     */
    
    /**
     * @Route("/situation/affichersituation", name="affichersituation")
     */
    public function affichersituation(Request $req): Response
    {   
        $etu = $this->getUser();
        
        $situ = $etu->getSituations();
		
		$formAjoutSituation = $this->createForm(BoutonAjoutSituationType::class);
		$formAjoutSituation->handleRequest($req);
        
		if($formAjoutSituation->get('ajout')->isClicked()){
            return $this->redirectToRoute('ajoutsituation');
        }
        if($formAjoutSituation->get('retour')->isClicked()){
			return $this->redirectToRoute('acceuil');
		}
		
        return $this->render('situation/affichersituation.html.twig', [
            'situations' => $situ,
            'formajout' => $formAjoutSituation->createView(),
        ]);
    }
    
    /**
     * Permet l'ajout d'une nouvelle situation
     *
     * @param req 
     *
     * @return Vue
     * @author Louis Boussarie
     * @version 1.0
     */

    /**
     * @Route("/situation/ajoutsituation", name="ajoutsituation")
     */
    public function ajoutsituation(Request $req): Response
    {
        $doctrine = $this->getDoctrine();
    	$manager = $doctrine->getManager();
    	
    	$etu = $this->getUser();

        $repoSituation = $manager->getRepository(Situation::class);
        $situations = $repoSituation->findAll();

        $situation = new Situation($etu,count($situations)+1);
        $form = $this->createForm(SituationType::class,$situation);
        $form->handleRequest($req);

        if($form->isSubmitted() && $form->isValid()){
            if($form->get('retour')->isClicked()){
                $reponse = $this->redirectToRoute('affichersituation');
            }
            else {
                $manager->persist($situation);
                $manager->flush();
                
                $reponse = $this->redirectToRoute('affichersituation');
                
                if($form->get('activite')->isClicked()){
                    $reponse = $this->redirectToRoute('ajoutactivitesitu', ['id' => $situation->getId()]);
                }
            }
        }
        else {   
            $reponse = $this->render('situation/ajoutsituation.html.twig', [
                'form' => $form->createView(),
            ]);
        }
        return $reponse;
    }

    /**
     * @Route("/situation/supprimersituation/{id}", name="supprimersituation")
     */
    public function supprimersituation(int $id): Response
    {
        $doctrine = $this->getDoctrine();
    	$manager = $doctrine->getManager();
    	$repoSitu = $manager->getRepository(Situation::class);
        $situation = $repoSitu->find($id);
        
        $listereformulation = $situation->getReformulations();
        
        foreach($listereformulation as $r){
            $manager->remove($r);
        }
		
		$listeproduction = $situation->getProductions();
		
		foreach($listeproduction as $p){
			$manager->remove($p);
		}
        
        $manager->remove($situation);
        $manager->flush();

        return $this->redirectToRoute('affichersituation');
    }
    
    /**
     * @Route("/situation/modifiersituation/{id}", name="modifiersituation")
     */
    public function modifiersituation(Request $req,int $id): Response
    {
    	$doctrine = $this->getDoctrine();
    	$manager = $doctrine->getManager();
    	$repoSitu = $manager->getRepository(Situation::class);
    	$situation = $repoSitu->find($id);
    	
    	$form = $this->createForm(SituationType::class,$situation);
    	$form->handleRequest($req);
    	
    	if($form->isSubmitted() && $form->isValid()){
    	    if($form->get('retour')->isClicked()){
    	        $reponse = $this->redirectToRoute('affichersituation');
    	    }
    	    if($form->get('activite')->isClicked()){
    	        $reponse = $this->redirectToRoute('ajoutactivitesitu', ['id' => $situation->getId()]);
    	    }
    	    else {
    	        $manager->persist($situation);
    	        $manager->flush();

                if($this->getUser()->getRoles() == 'ROLE_ETUD') {
                    $reponse = $this->redirectToRoute('affichersituation');
                }
                else {
                    $reponse = $this->redirectToRoute('affichersitueleve', ['id' => $situation->getEtudiant()->getId()]);
                }
    	    }
    	}
    	else {
    	    $reponse = $this->render('situation/modifiersituation.html.twig', ['form' => $form->createView()]);
    	}
    	
    	return $reponse;
    }
    
    /**
     * @Route("/situation/affichersitueleve/{id}", name="affichersitueleve")
     */
    public function affichersitueleve(Request $req,int $id): Response
    {
        
        $doctrine = $this->getDoctrine();
        $manager = $doctrine->getManager();
        $repoEtu = $manager->getRepository(Etudiant::class);
        $etudiant = $repoEtu->find($id);
        $situations = $etudiant->getSituations();
        
        
        $form = $this->createForm(ListeSituationType::class);
        $form->handleRequest($req);
        
        if($form->isSubmitted() && $form->isValid()){
            if($form){
                return $this->redirectToRoute('modifiersituation', array('id' => $form->get('modifier')->getData()));
            }
        }
        return $this->render('situation/affichersituation.html.twig', [
            'situations' => $situations,
            'form' => $form->createView(),
        ]);
    }
    
    /**
     * @Route("/situation/ajoutcommentaire/{id}", name="ajoutcommentaire")
     */
    public function ajoutcommentaire(Request $req,int $id): Response
    {
        $situations = [];
        $reformulations = [];
        $doctrine = $this->getDoctrine();
        $manager = $doctrine->getManager();
        $repoEtu = $manager->getRepository(Etudiant::class);
        $etu = $repoEtu->find($id);
        
        $situations = $etu->getSituations();
        
        foreach($situations as $s) {
            $reformulations [$s->getId()] = $s->getReformulations();
			
			foreach($reformulations[$s->getId()] as $r){
				$form = $this->createForm(ReformulationsType::class,$r);
				$form->handleRequest($req);
				
				$listereformulations = $form->createView();
				
				if($form->isSubmitted() && $form->isValid()){
					$manager->persist($r);
					$manager->flush();
            
					$reponse = $this->redirectToRoute('affichersituationeleve');
				}
			}
        }
            $reponse = $this->render('situation/affichersituation.html.twig', [
				'listereformulation' => $listereformulations,
                'reformulations' => $reformulations,
				'situation' => $situations,
            ]);
        
        return $reponse;
    }
}
