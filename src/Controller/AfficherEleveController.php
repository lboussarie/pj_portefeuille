<?php

namespace App\Controller;

use App\Entity\Utilisateur;
use App\Form\AjoutUtilisateurType;
use App\Form\BtnUtilisateurType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use App\Entity\Etudiant;
use App\Entity\Professeur;

class AfficherEleveController extends AbstractController
{
    
    
    /**
     * @Route("/afficher/eleves", name="list_utilisateur")
     */
    public function listUtilisateur(Request $req)
    {
        $form = $this->createForm(BtnUtilisateurType::class);
        $form->handleRequest($req);
        
        $doc = $this->getDoctrine();
        $man = $doc->getManager();
        $repo = $man->getRepository(Etudiant::class);
        $liste = $repo->findAll();
        
        if ($form->isSubmitted()) {
            if ($form->get('addUser')->isClicked())  {
                $res = $this->redirectToRoute('ajout_utilisateur');
            }
        }
        else {
            $res = $this->render('afficher_eleve/list.html.twig', [
                'eleves' => $liste,
                'form' => $form->createView(),
                
            ]);
        }
        
       return $res;
        
    }
    
    /**
     * @Route("/ajout/utilisateur", name="ajout_utilisateur")
     */
    public function AjoutUtilisateur(Request $request, UserPasswordHasherInterface $hasher): Response
    {
        $doc = $this->getDoctrine();
        $man = $doc->getManager();
        
        $form = $this->createForm(AjoutUtilisateurType::class);
        $form->handleRequest($request);
        
        if ($form->isSubmitted()) {
            $file = $form->get('file');
            $choice = $form->get('choix');
            
            $encoders = [new CsvEncoder()];
            $normalizers = [new ObjectNormalizer()];
            $serializers = new Serializer($normalizers, $encoders);
            
            foreach ($serializers->decode(file_get_contents($file->getData()), 'csv') as $r) {
                $data[] = $r;
            }
            
            
            if ($choice->getData() == 0) {
                foreach ($data as $etud) {
                    $etudiant = (new Etudiant())
                    ->setNom($etud['nom'])
                    ->setPrenom($etud['prenom'])
                    ->setEmail($etud['email']);
                    
                    $etudiant->setPassword($hasher->hashPassword($etudiant, $data['password']));
                    
                    $etudiant->setRoles(['["ROLE_ETUD"]']);
                    
                    $man->persist($etudiant);
                }       
                
            } elseif ($choice->getData() == 1) {
                foreach ($data as $prof) {
                    $professeur = (new Professeur())
                    ->setNom($prof['nom'])
                    ->setPrenom($prof['prenom'])
                    ->setEmail($prof['email']);
                    
                    $professeur->setPassword($hasher->hashPassword($professeur, $data['password']));
                    
                    $professeur->setRoles(['["ROLE_PROF"]']);
                    
                    $man->persist($professeur);
                }            
            }
            $man->flush();
            $ret = $this->redirectToRoute('list_utilisateur');
            
        }
        else {
            $ret = $this->render('afficher_eleve/ajout.html.twig', ['form' => $form->createView()]);
        }
        
        return $ret;
    }
    
    
    /**
     * @Route("/synthesePDF", name="synthesePdf")
     */
    public function synthesePDF(Request $req)
    {
        return $this->render('afficher_eleve/synthese.html.twig', [
            'controller_name' => 'ControllerUserController',
        ]);
        
    }
    
    
    /*Import fichier CSV dans une BDD */
    /*LOAD DATA INFILE 'fichier.csv'
     INTO TABLE utilisateur CHARACTER SET 'utf8' FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 LINES;*/
}
