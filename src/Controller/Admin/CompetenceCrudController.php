<?php

namespace App\Controller\Admin;

use App\Entity\Competence;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CompetenceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Competence::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->showEntityActionsAsDropdown(false)
            ->setPageTitle('index', '%entity_label_plural%')
            ->setPageTitle('new', 'Création - %entity_label_singular%')
            ->setPageTitle('edit', "Modifier - %entity_label_singular%")
            ->setPageTitle('detail', "Détails - %entity_label_singular%")
            ->setEntityLabelInSingular('compétence')
            ->setEntityLabelInPlural('Compétences')
            ->setDateFormat('d/M/Y')
            ;
    }

    public function configureFields(string $pageName): iterable
    {
        yield TextField::new('nom')
            ->setColumns(6);

        yield TextField::new('description')
            ->setColumns(6);
    }

    public function configureActions(Actions $actions): Actions
    {
        $actions->remove(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER)
            ->remove(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE);
        return $actions;
    }
}
