<?php

namespace App\Controller\Admin;

use App\Entity\Activite;
use App\Entity\Competence;
use App\Entity\Parcours;
use App\Entity\Processus;
use App\Entity\Promotion;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     * @IsGranted("ROLE_PROF")
     */
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Projet');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linktoRoute('Voir le site', 'fa fa-fw fa-eye', 'accueil');
        yield MenuItem::section('');
        yield MenuItem::linkToCrud('Parcours', 'fas fa-list', Parcours::class);
        yield MenuItem::linkToCrud('Processus', 'fas fa-list', Processus::class);
        yield MenuItem::linkToCrud('Promotions', 'fas fa-list', Promotion::class);
        yield MenuItem::linkToCrud('Activités', 'fas fa-list', Activite::class);
        yield MenuItem::linkToCrud('Compétences', 'fas fa-list', Competence::class);
    }
}
