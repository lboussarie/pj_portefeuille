<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Promotion;
use App\Form\PromotionType;

class PromotionController extends AbstractController
{
    /**
     * @Route("/promotion/ajoutpromotion", name="ajoutpromotion")
     */
    public function ajoutpromotion(Request $req): Response
    {
        $doctrine = $this->getDoctrine();
        $manager = $doctrine->getManager();
        
        $promotion = new Promotion();
        $form = $this->createForm(PromotionType::class,$promotion);
        $form->handleRequest($req);
        
        if($form->isSubmitted() && $form->isValid()){
            if($form->get('retour')->isClicked()){
                $reponse = $this->redirectToRoute('gestionpromotion');
            }
            else {
                $manager->persist($promotion);
                $manager->flush();
                
                $reponse = $this->redirectToRoute('gestionpromotion');
            }
        }
        else {
            $reponse = $this->render('promotion/ajoutpromotion.html.twig', [
                'form' => $form->createView(),
            ]);
        }
        return $reponse;
    }
    
    /**
     * @Route("/promotion/gestionpromotion", name="gestionpromotion")
     */
    public function gestionpromotion(Request $req): Response
    {
        $doctrine = $this->getDoctrine();
        $manager = $doctrine->getManager();
        $repoParc = $manager->getRepository(Promotion::class);
        $promotion = $repoParc->findAll();
        
        return $this->render('promotion/gestionpromotion.html.twig', ['promotion' => $promotion]);
    }
    
    /**
     *  @Route("/parcours/modifierpromotions/{id}", name="modifierpromotions")
     */
    public function modifierpromotions(Request $req,int $id) {
        
        $doctrine = $this->getDoctrine();
        $manager = $doctrine->getManager();
        $repoParc = $manager->getRepository(Promotion::class);
        $promotion = $repoParc->find($id);
        
        $form = $this->createForm(PromotionType::class,$promotion);
        $form->handleRequest($req);
        
        if($form->isSubmitted() && $form->isValid()){
            if($form->get('retour')->isClicked()){
                $reponse = $this->redirectToRoute('gestionpromotion');
            }
            else {
                $manager->persist($promotion);
                $manager->flush();
                
                $reponse = $this->redirectToRoute('gestionpromotion');
            }
        }
        else {
            $reponse = $this->render('promotion/modifierpromotion.html.twig', ['form' => $form->createView()]);
        }
        
        return $reponse;
    }
    
    /**
     *  @Route("/parcours/supprimerpromotion/{id}", name="supprimerpromotion")
     */
    public function supprimerpromotion(Request $req,int $id) {
        
        $etudiants = [];
        $doctrine = $this->getDoctrine();
        $manager = $doctrine->getManager();
        $repoParc = $manager->getRepository(Promotion::class);
        $promotion = $repoParc->find($id);
        
        $etudiants = $promotion->getEtudiant();
        
        foreach ($etudiants as $etu){
            $promotion->removeEtudiant($etu);
        }
        
        $manager->remove($promotion);
        $manager->flush();
        
        return $this->redirectToRoute('gestionpromotion');
    }
}
