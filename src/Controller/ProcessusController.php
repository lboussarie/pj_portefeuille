<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Processus;
use App\Entity\Activite;
use App\Repository\ProcessusRepository;
use App\Entity\Competence;
use App\Form\ActiviteType;

class ProcessusController extends AbstractController
{
    /**
     * @Route("/processus", name="processus")
     */
    public function index(): Response
    {
        return $this->render('processus/index.html.twig', [
            'controller_name' => 'ProcessusController',
        ]);
    }
    
    /**
     * @Route("/gestionprocessus", name="gestionprocessus")
     */
    public function gestionprocessus(): Response
    {
        $listeActivites=[];
        $parcours=[];
        $competences = [];
        
        $doc = $this->getDoctrine();
        $man = $doc->getManager();
        $repoP = $man->getRepository(Processus::class);
        $repoA = $man->getRepository(Activite::class);
        $repoC = $man->getRepository(Competence::class);
        
        // on récupère les processus
        $listeProcessus = $repoP->findAll();
        
        foreach ($listeProcessus as $p){
            $listeActivites[$p->getId()] = $repoA->findBy(['processus' => $p->getId()]);
            $parcours [$p->getId()] = $p->getParcours();
            
            foreach ($listeActivites[$p->getId()] as $act){
                $competences[$act->getId()] = $repoC->findBy(['activite' => $act->getId()]);
            }
        }
        
        return $this->render('processus/liste.html.twig', [
            'listeProcessus' => $listeProcessus,
            'listeActivites' => $listeActivites,
            'parcours' => $parcours,
            'competences' => $competences,
        ]);
    }
}
