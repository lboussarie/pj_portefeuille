<?php

namespace App\Controller;

use App\Entity\Activite;
use App\Entity\Competence;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\CompetenceType;

class CompetenceController extends AbstractController
{
    /**
     * @Route("/competence", name="competence")
     */
    public function index(): Response
    {
        return $this->render('competence/index.html.twig', [
            'controller_name' => 'CompetenceController',
        ]);
    }
    
    /**
     * @Route("/gestionCompetence/{id}", name="gestionCompetence")
     */
    public function gestionprocessus(int $id): Response
    {
        $competences = [];
        
        $activites = new Activite();
        $doc = $this->getDoctrine();
        $man = $doc->getManager();
        $repoA = $man->getRepository(Activite::class);
        $repoC = $man->getRepository(Competence::class);

        $activites=$repoA->find($id);
        
        $competences= $repoC->findBy(['activite'=>($activites->getId())]);
        
        
        return $this->render('competence/liste.html.twig', [
            'activites' => $activites,
            'competences' => $competences,
        ]);
    }
    
    /**
     * @Route("/ajoutCompetence", name="ajoutCompetence")
     */
    public function ajoutCompetence(Request $req): Response
    {
        
        $doc = $this->getDoctrine();
        $man = $doc->getManager();
        
        //construction de la liste des processus
        $repoA = $man->getRepository(Activite::class);
        $listeActivite = $repoA->findAll();
        
        $c = new Competence();
        $form = $this->createForm(CompetenceType::class, $c, ['listeActivite'=>$listeActivite]);
        $form->handleRequest($req);
        if ($form->isSubmitted()) {
            //traitement données
            $man->persist($c);
            $man->flush();
            
            $ret = $this->redirectToRoute('gestionprocessus');
            
        } else {
            //formulaire non validé donc affichage formulaire
            $ret = $this->render('competence/add.html.twig', [
                'form' => $form->createView(),
            ]);
        }
        
        return $ret;
        
        $form = $this->createForm(CompetenceType::class);
    }
    
    /**
     * @Route("/editCompetence/{id}", name="editCompetence")
     */
    public function editCompetence(Request $req, int $id): Response
    {
        
        $doc = $this->getDoctrine();
        $man = $doc->getManager();
        
        $repoC=$man->getRepository(Competence::class);
        $c = $repoC->find($id);
        
        //construction de la liste des processus
        $repoA = $man->getRepository(Activite::class);
        $listeActivite = $repoA->findAll();

        $form = $this->createForm(CompetenceType::class, $c, ['listeActivite'=>$listeActivite]);
        $form->handleRequest($req);
        if ($form->isSubmitted()) {
            //traitement données
            $man->persist($c);
            $man->flush();
            
            $ret = $this->redirectToRoute('gestionprocessus');
            
        } else {
            //formulaire non validé donc affichage formulaire
            $ret = $this->render('competence/edit.html.twig', [
                'form' => $form->createView(),
                'c' => $c,
            ]);
        }
        
        return $ret;
        
        $form = $this->createForm(CompetenceType::class);
    }
    
    /**
     * @Route("/ajoutCompetencewId/{id}", name="ajoutCompetencewId")
     */
    public function ajoutCompetencewId(Request $req, int $id): Response
    {
        
        $doc = $this->getDoctrine();
        $man = $doc->getManager();
        
        //construction de la liste des processus
        $repoA = $man->getRepository(Activite::class);
        $activite = $repoA->find($id);
        
        $c = new Competence();
        $c->setActivite($activite);
        
        $form = $this->createForm(CompetenceType::class, $c);
        $form->handleRequest($req);
        if ($form->isSubmitted()) {
            //traitement données
            $man->persist($c);
            $man->flush();
            
            $ret = $this->redirectToRoute('gestionprocessus');
            
        } else {
            //formulaire non validé donc affichage formulaire
            $ret = $this->render('competence/add.html.twig', [
                'form' => $form->createView(),
            ]);
        }
        
        return $ret;
        
        $form = $this->createForm(CompetenceType::class);
    }
}
