<?php
/*chose à faire :
 * -vérifier authentification avec le hach
 * 
 * 
 */
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManager;
use App\Form\FormUserType;
use Symfony\Component\HttpFoundation\Request;
use PhpParser\Node\Expr\Isset_;
use App\Entity\Utilisateur;

class UserController extends AbstractController
{
    /**
     * @Route("/controller/user", name="controller_user")
     */
    public function index(): Response
    {
        return $this->render('controller_user/index.html.twig', [
            'controller_name' => 'ControllerUserController',
        ]);
    }
   
    public function connexion(Request $req): Response 
    {
       
        
        $user_chercher = new Utilisateur();
       
        //formulaire
        $form = $this->createForm(FormUserType::class,$user_chercher);
        $form->Handlerequest($req);
        
     
        if ($form->isSubmitted() ){
            
            
            //récupération du mail du formulaire
            $mail = $form->get('mail')->getData();
            
            //récupération du mdp du formulaire
            $mdp = $form->get('mdp')->getData();
            
            $mdp_hach =  password_hash($mdp, PASSWORD_DEFAULT);
            var_dump($mdp_hach);
            $doct = $this->getDoctrine();
            $man = $doct->getManager();
            
            //recherche du mail de l'utilisateur 
            $repoPers = $man->getRepository('\App\Entity\Utilisateur');
            $user_chercher = $repoPers->findOneBymail($mail);
            var_dump($user_chercher);
            var_dump($mail);
            
       
            //si mail incorrect
            if (!(isset($user_chercher))){
               
               //flash message for redirection
                $this->addFlash('error_mail', 'le mail est incorrect');                
               
               return  $this->redirectToRoute('login');
                
            }
           
           $mail_user_chercher = $user_chercher->getmail();
           $mdp_user_chercher = $user_chercher->getmdp();
           
            if($mdp_user_chercher != $mdp ){
                
               
                $this->addFlash('error_mdp', 'le mot de passe est incorrrect');
                
                return $this->redirectToRoute('login');
                
               
                
            }
            else{
                
                $response =  $this->render('controller_user/affichage.html.twig', [
                    'mail' => $mail, 'mdp' => $mdp, 'user' => $mail_user_chercher, 'mdp_base' => $mdp_user_chercher  ]);   
            }
            
            
        }else {
            
            $response = $this->render('controller_user/connexion.html.twig', [
                'form' => $form->createView() ]);
            
        }
        
        
        return $response;
       
      
     
    }
      
}
