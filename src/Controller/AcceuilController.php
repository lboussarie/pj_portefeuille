<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Promotion;
use App\Entity\Utilisateur;

class AcceuilController extends AbstractController
{
    /**
     * @Route("/accueil", name="accueil")
     */
    public function accueil(Request $req): Response
    {
        $doctrine = $this->getDoctrine();
        $manager = $doctrine->getManager();
        $repoPromo = $manager->getRepository(Promotion::class);
        $promotions = $repoPromo->findAll();
        $sesi = $req->getSession();
        $sesi->set('promotions', $promotions);
        
        return $this->render('base.html.twig');
    }
}
